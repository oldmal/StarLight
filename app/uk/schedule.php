<?php include 'components/head.php' ?>
	<title>Розклад проведення StarLight</title>
</head>
<body class="page-wrap schedule-page">
	
	<?php include 'components/header.php' ?>
	
	<div class="hero-2">
		<div class="hero-2__unit">
			<h1 class="hero-2__title"><img class="hero-2__title-img" alt="StarLight" src="../img/content/main-text.svg"></h1>
		</div>
		<div class="hero-2__unit">
			<h2 class="hero-2__subtitle"><img class="hero-2__subtitle-img" alt="Grand Prix Cup" src="../img/content/secondary-text.svg"></h2>
		</div>
	</div>

	<main class="main">
		<section class="schedule" id="schedule">
			<h3 class="schedule__title">Розклад</h3>
			<div class="schedule__units">
				<div class="schedule__unit">
					<div class="schedule__date">
						<p class="schedule__day">9</p>
						<p class="schedule__month">лютого</p>
					</div>
					<div class="schedule__items">
						<div class="schedule__item">
							<p class="schedule__item-time">9:00</p>
							<h3 class="schedule__item-title">Перше відділення</h3>
							<table class="schedule__item-table">
								<tr>
									<th title="Номер групи">№</th>
									<th>Категорія</th>
									<th>Танець</th>
								</tr>
								<tr>
									<td>5</td>
									<td>Молодь 1+2 RS</td>
									<td>St</td>
								</tr>
								<tr>
									<td>8</td>
									<td>Юніори 2 C</td>
									<td>Lat</th>
								</tr>
								<tr>
									<td>11</td>
									<td>Юніори 1 C</td>
									<td>St</td>
								</tr>
								<tr>
									<td>13</td>
									<td>Юніори 1 E</td>
									<td>St</td>
								</tr>
								<tr>
									<td>16</td>
									<td>Ювенали 2 E</td>
									<td>St</td>
								</tr>
								<tr>
									<td>19</td>
									<td>Ювенали 1 E</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>20</td>
									<td>Ювенали 1 H</td>
									<td>W,Q,Ch,J</td>
								</tr>
								<tr>
									<td>21</td>
									<td>Ювенали 1 Школа</td>
									<td>W,Ch,J</td>
								</tr>
								<tr>
									<td>24</td>
									<td>Діти Дебют (соло) до 8 років</td>
									<td>W,Ch</td>
								</tr>
							</table>
						</div>
						<div class="schedule__item">
							<p class="schedule__item-time">13:00</p>
							<h3 class="schedule__item-title">Друге відділення</h3>
							<table class="schedule__item-table">
								<tr>
									<th title="Номер групи">№</th>
									<th>Категорія</th>
									<th>Танець</th>
								</tr>
								<tr>
									<td>2</td>
									<td>Дорослі RS</td>
									<td>St</td>
								</tr>
								<tr>
									<td>7</td>
									<td>Юніори 2 RS</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>10</td>
									<td>Юніори 1 RS</td>
									<td>St</td>
								</tr>
								<tr>
									<td>12</td>
									<td>Юніори 1 D</td>
									<td>St</td>
								</tr>
								<tr>
									<td>16</td>
									<td>Ювенали 2 E</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>19</td>
									<td>Ювенали 1 E</td>
									<td>St</td>
								</tr>
								<tr>
									<td>26</td>
									<td>BIG Dance Pro-Am Cup (Scolarship) A(17-35)</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>27</td>
									<td>BIG Dance Pro-Am Cup (Scolarship) B(36+)</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>28</td>
									<td>BIG Dance Pro-Am Cup (Single dance) A(17-35)</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>29</td>
									<td>BIG Dance Pro-Am Cup (Single dance) B(36+)</td>
									<td>Lat</td>
								</tr>
							</table>
						</div>
						<div class="schedule__item">
							<p class="schedule__item-time">18:00</p>
							<h3 class="schedule__item-title">Треттє відділення</h3>
							<table class="schedule__item-table">
								<tr>
									<th title="Номер групи">№</th>
									<th>Категорія</th>
									<th>Танець</th>
								</tr>
								<tr>
									<td>1</td>
									<td>Дорослі</td>
									<td>St</td>
								</tr>
								<tr>
									<td>3</td>
									<td>Молодь 2</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>4</td>
									<td>Молодь 1</td>
									<td>St</td>
								</tr>
								<tr>
									<td>6</td>
									<td>Юніори 2</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>9</td>
									<td>Юніори 1</td>
									<td>St</td>
								</tr>
								<tr>
									<td>14</td>
									<td>Ювенали 1+2</td>
									<td>Lat</td>
								</tr>
							</table>
						</div>
					</div>
				</div>

				<div class="schedule__unit">
					<div class="schedule__date">
						<p class="schedule__day">10</p>
						<p class="schedule__month">лютого</p>
					</div>
					<div class="schedule__items">
						<div class="schedule__item">
							<p class="schedule__item-time">9:00</p>
							<h3 class="schedule__item-title">Перше відділення</h3>
							<table class="schedule__item-table">
								<tr>
									<th title="Номер групи">№</th>
									<th>Категорія</th>
									<th>Танець</th>
								</tr>
								<tr>
									<td>5</td>
									<td>Молодь 1+2 RS</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>11</td>
									<td>Юніори 1 C</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>13</td>
									<td>Юніори 1 E</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>15</td>
									<td>Ювенали 2 D</td>
									<td>St</td>
								</tr>
								<tr>
									<td>17</td>
									<td>Ювенали 2 H</td>
									<td>W,Q,Ch,J</td>
								</tr>
								<tr>
									<td>18</td>
									<td>Ювенали 2 Школа</td>
									<td>W,Ch,J</td>
								</tr>
								<tr>
									<td>22</td>
									<td>Ювенали 1(соло) Школа</td>
									<td>W,Ch</td>
								</tr>
								<tr>
									<td>23</td>
									<td>Ювенали 2 (соло) Н</td>
									<td>W,Q,Ch,J</td>
								</tr>
								<tr>
									<td>25</td>
									<td>Діти Дебют до 8 років</td>
									<td>W,Ch</td>
								</tr>
							</table>
						</div>
						<div class="schedule__item">
							<p class="schedule__item-time">13:00</p>
							<h3 class="schedule__item-title">Друге відділення</h3>
							<table class="schedule__item-table">
								<tr>
									<th title="Номер групи">№</th>
									<th>Категорія</th>
									<th>Танець</th>
								</tr>
								<tr>
									<td>2</td>
									<td>Дорослі RS</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>7</td>
									<td>Юніори 2 RS</td>
									<td>St</td>
								</tr>
								<tr>
									<td>8</td>
									<td>Юніори 2 C</td>
									<td>St</td>
								</tr>
								<tr>
									<td>10</td>
									<td>Юніори 1 RS</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>12</td>
									<td>Юніори 1 D</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>15</td>
									<td>Ювенали 2 D</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>26</td>
									<td>BIG Dance Pro-Am Cup (Scolarship) A(17-35)</td>
									<td>St</td>
								</tr>
								<tr>
									<td>27</td>
									<td>BIG Dance Pro-Am Cup (Scolarship) B(36+)</td>
									<td>St</td>
								</tr>
								<tr>
									<td>28</td>
									<td>BIG Dance Pro-Am Cup (Single dance) A(17-35)</td>
									<td>St</td>
								</tr>
								<tr>
									<td>29</td>
									<td>BIG Dance Pro-Am Cup (Single dance) B(36+)</td>
									<td>St</td>
								</tr>
							</table>
						</div>
						<div class="schedule__item">
							<p class="schedule__item-time">18:00</p>
							<h3 class="schedule__item-title">Треттє відділення</h3>
							<table class="schedule__item-table">
								<tr>
									<th title="Номер групи">№</th>
									<th>Категорія</th>
									<th>Танець</th>
								</tr>
								<tr>
									<td>1</td>
									<td>Дорослі</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>3</td>
									<td>Молодь 2</td>
									<td>St</td>
								</tr>
								<tr>
									<td>4</td>
									<td>Молодь 1</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>6</td>
									<td>Юніори 2</td>
									<td>St</td>
								</tr>
								<tr>
									<td>9</td>
									<td>Юніори 1</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>14</td>
									<td>Ювенали 1+2</td>
									<td>St</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="schedule__gal" data-stopautoplayontouch="false" data-autoplay="true" data-loop="true" data-fit="cover">
				<?php for ($scheduleNum = 1; $scheduleNum <= 5; $scheduleNum++) { ?>
					<a href="../img/schedule/sch-slide-<?=$scheduleNum?>.jpg?v=<?=$ver?>"></a>
				<?php }; ?>
			</div>
		</section>

		<section class="fee">
			<h3 class="schedule__subtitle">Стартові внески</h3>
			<table class="fee__table">
				<tr>
					<th>Група 1-11, 14</th>
					<td>400 грн. з учасника</td>
				</tr>
				<tr>
					<th>Група 12, 13, 15, 16, 19</th>
					<td>350 грн. з учасника</td>
				</tr>
				<tr>
					<th>Група 17, 20, 23</th>
					<td>300 грн. з учасника</td>
				</tr>
				<tr>
					<th>Група 18, 21, 22, 24, 25</th>
					<td>250 грн. з учасника</td>
				</tr>
				<tr>
					<th>Група 26-29</th>
					<td>650 грн. з учасника</td>
				</tr>
			</table>
		</section>
		<section class="additional">
			<div class="additional__inner">
				<div>
					<h4 class="additional__title">ProAm інформація</h4>
					<span>Валерія</span>
					<a href="tel:+380932098686">+38 (093) 209-86-86</a>
					<a href="mailto:BIGProAmDance@gmail.com">BIGProAmDance@gmail.com</a>
				</div>
				<div>
					<h4 class="additional__title">Вхідні квитки</h4>
					<!-- <p>200 грн. на перше відділення, 250 грн. на друге та третє відділення.</p> -->
					<p>1 відділення - 100 грн. / місце за столиком + квиток - 200 грн.</p>
					<p>2 відділення - 150 грн. / місце за столиком + квиток - 250 грн.</p>
					<p>3 відділення - 200 грн. / місце за столиком + квиток - 400 грн.</p>
				</div>
				<div>
					<h4 class="additional__title">Бронювання столиків</h4>
					<span>Юля</span>
					<a href="tel:+380991337729">+38 (099) 133-77-29</a>
				</div>
				<div>
					<h4 class="additional__title">Бронювання уроків</h4>
					<span>Юля</span>
					<a href="tel:+380637026994">+38 (063) 702-69-94</a>
				</div>
				<div>
					<h4 class="additional__title">Реєстрація</h4>
					<span>Не пізніше – 07.02.2019</span>
					<a href="https://flymark.com.ua/Competition/Details/951">ЗАРЕЄСТРУВАТИСЬ</a>
					<p><em>Без попередньої заявки участь можлива тільки за умови сплати подвійного внеску!!! Обов’язкова попередня реєстрація для київських пар відбуватиметься тільки у п’ятницю 8-го лютого 2019 р. з 11.00 до 20.00 за адресою:<br>м. Київ, вул. Вадима Гетьмана 6, ТРЦ "Космополіт", 4 поверх, зал "Хичкок"</em></p>
				</div>
			</div>
		</section>

		<!-- <section class="prize">
			<h3 class="schedule__subtitle">Призовий фонд</h3>
			<table class="prize__table">
				<tr>
					<th>Категорія</th>
					<th>1</th>
					<th>2</th>
					<th>3</th>
					<th>4</th>
					<th>5</th>
					<th>6</th>
				</tr>
				<tr>
					<th>Дорослі</th>
					<td>4000</td>
					<td>3000</td>
					<td>2000</td>
					<td>1000</td>
					<td>1000</td>
					<td>1000</td>
				</tr>
				<tr>
					<th>Молодь 2</th>
					<td>3000</td>
					<td>2000</td>
					<td>1500</td>
					<td>700</td>
					<td>600</td>
					<td>600</td>
				</tr>
				<tr>
					<th>Молодь 1</th>
					<td>3000</td>
					<td>2000</td>
					<td>1500</td>
					<td>700</td>
					<td>600</td>
					<td>600</td>
				</tr>
				<tr>
					<th>Юніори 2</th>
					<td>П</td>
					<td>П</td>
					<td>П</td>
					<td>П</td>
					<td>П</td>
					<td>П</td>
				</tr>
				<tr>
					<th>Юніори 1</th>
					<td>П</td>
					<td>П</td>
					<td>П</td>
					<td>П</td>
					<td>П</td>
					<td>П</td>
				</tr>
				<tr>
					<th>Ювенали 2</th>
					<td>П</td>
					<td>П</td>
					<td>П</td>
					<td>П</td>
					<td>П</td>
					<td>П</td>
				</tr>
				<tr>
					<th>Ювенали 1</th>
					<td>П</td>
					<td>П</td>
					<td>П</td>
					<td>П</td>
					<td>П</td>
					<td>П</td>
				</tr>
				<tr>
					<td colspan="7">П – призи від торгового дому “Grand Prix”</td>
				</tr>
			</table>
		</section> -->

		<section class="additional rules">
			<div class="additional__inner">
				<h4 class="additional__title">Правила</h4>
				<span>Згідно правил WDC</span>

				<h4 class="additional__title">Організатори</h4>
				<div>
					<span>Миркін Роман</span>
					<a href="tel:+380503345487">+38 (050) 334-54-87‬</a>‎
					<a href="mailto:wdcalukraine@ukr.net">wdcalukraine@ukr.net</a>
				</div>
				<div>
					<span>Буланий Максим</span>
					<a href="tel:+380504730066">+38 (050) 473-00-66</a>‎
				</div>
			</div>
		</section>

	</main>
	
	<?php include 'components/footer.php' ?>

</body>
</html>






