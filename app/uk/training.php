<?php include 'components/head.php' ?>
	<title>Тренувальні збори StarLight</title>
</head>
<body class="page-wrap training-page">
	
	<?php include 'components/header.php' ?>
	
	<div class="hero-2">
		<div class="hero-2__unit">
			<h1 class="hero-2__title"><img class="hero-2__title-img" alt="StarLight" src="../img/content/main-text.svg"></h1>
		</div>
		<div class="hero-2__unit">
			<h2 class="hero-2__subtitle"><img class="hero-2__subtitle-img" alt="Grand Prix Cup" src="../img/content/secondary-text.svg"></h2>
		</div>
	</div>

	<main class="main">
		<section class="tng" id="tng">
			<h3 class="tng__title">Тренувальні збори 7-8 лютого</h3>
			<p class="tng__text">Тренувальні збори з найкращими тренерами світу. Групові та приватні уроки.</p>
			<div class="tng__units">
				<div class="tng__unit">
					<div class="tng__item">
						<img class="tng__item-img" src="../img/coaches/ryupin.jpg?v=<?=$ver?>" alt="Сергій Рюпін">
						<p class="tng__item-text">Сергій Рюпін</p>
					</div>
					<div class="tng__item">
						<img class="tng__item-img" src="../img/coaches/pino.jpg?v=<?=$ver?>" alt="Вільям Піно">
						<p class="tng__item-text">Вільям Піно</p>
					</div>
					<div class="tng__item">
						<img class="tng__item-img" src="../img/coaches/fletcher.jpg?v=<?=$ver?>" alt="Аллан Флетчер">
						<p class="tng__item-text">Аллан Флетчер</p>
					</div>

					<!-- <div class="tng__item">
						<img class="tng__item-img" src="../img/coaches/toft.jpg?v=<?=$ver?>" alt="Вибеке Тофт">
						<p class="tng__item-text">Вибеке Тофт</p>
					</div>
					<div class="tng__item">
						<img class="tng__item-img" src="../img/coaches/boyce.jpg?v=<?=$ver?>" alt="Уоррен Бойс">
						<p class="tng__item-text">Уоррен Бойс</p>
					</div> -->
				</div>
				<div class="tng__unit">
					<div class="tng__item">
						<img class="tng__item-img" src="../img/coaches/short.jpg?v=<?=$ver?>" alt="Крістофєр Шорт">
						<p class="tng__item-text">Крістофєр Шорт</p>
					</div>
					<div class="tng__item">
						<img class="tng__item-img" src="../img/coaches/hawkins.jpg?v=<?=$ver?>" alt="Крістофер Хокінс">
						<p class="tng__item-text">Крістофер Хокінс</p>
					</div>
					<div class="tng__item">
						<img class="tng__item-img" src="../img/coaches/kryklyvyy.jpg?v=<?=$ver?>" alt="Слава Крикливий">
						<p class="tng__item-text">Слава Крикливий</p>
					</div>
					
					<!-- <div class="tng__item">
						<img class="tng__item-img" src="../img/coaches/fancello.jpg?v=<?=$ver?>" alt="Сімона Фанчело">
						<p class="tng__item-text">Сімона Фанчело</p>
					</div>
					<div class="tng__item">
						<img class="tng__item-img" src="../img/coaches/marriner.jpg?v=<?=$ver?>" alt="Лін Мерінер">
						<p class="tng__item-text">Лін Мерінер</p>
					</div> -->
				</div>
				<div class="tng__unit">
					<div class="tng__item">
						<img class="tng__item-img" src="../img/coaches/colantoni.jpg?v=<?=$ver?>" alt="Валеріо Колантоні">
						<p class="tng__item-text">Валеріо Колантоні</p>
					</div>
					<div class="tng__item">
						<img class="tng__item-img" src="../img/coaches/smagin.jpg?v=<?=$ver?>" alt="Евгений Смагин">
						<p class="tng__item-text">Евгений Смагин</p>
					</div>
					<!-- <div class="tng__item">
						<img class="tng__item-img" src="../img/coaches/emrich.jpg?v=<?=$ver?>" alt="Ханес Емріх">
						<p class="tng__item-text">Ханес Емріх</p>
					</div>
					<div class="tng__item">
						<img class="tng__item-img" src="../img/coaches/kongsdal.jpg?v=<?=$ver?>" alt="Клаус Конгсдал">
						<p class="tng__item-text">Клаус Конгсдал</p>
					</div>
					<div class="tng__item">
						<img class="tng__item-img" src="../img/coaches/vescovo.jpg?v=<?=$ver?>" alt="Маурицио Весково">
						<p class="tng__item-text">Маурицио Весково</p>
					</div> -->
				</div>
			</div>
			<div class="tng__text">
				<span>Бронь уроків та інформація за тел. </span>
				<a class="tng__phone" href="tel:+380637026994">+38 (063) 702-69-94</a>
				<span>(Юля)</span><br> або
				<a class="tng__phone" href="tel:+380504747274">+38 (050) 474-72-74</a>
				<span>(Катерина)</span>
				<br>
				<a class="tng__email" href="mailto:katyamail@ukr.net">katyamail@ukr.net</a>
			</div>
			<!-- <iframe class="tng__video" src="https://www.youtube.com/embed/ptg82f53yOs" allowfullscreen></iframe> -->
			<iframe class="tng__video" src="https://www.youtube.com/embed/tRvIbwfTb-c?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
			
			<!-- <div class="tng__gallery tng__gallery--hidden grid" id="tngGallery">
				<img class="grid-item" src="../img/tng/slide-1.jpg?v=<?=$ver?>" alt="">	
				<img class="grid-item" src="../img/tng/slide-2.jpg?v=<?=$ver?>" alt="">	
				<img class="grid-item" src="../img/tng/slide-3.jpg?v=<?=$ver?>" alt="">	
				<img class="grid-item" src="../img/tng/slide-4.jpg?v=<?=$ver?>" alt="">	
				<img class="grid-item" src="../img/tng/slide-5.jpg?v=<?=$ver?>" alt="">	
				<img class="grid-item" src="../img/tng/slide-6.jpg?v=<?=$ver?>" alt="">
			</div> -->
			
			<div class="tng__schedule">
				<h3 class="schedule__subtitle">Розклад тренувальный зборів</h3>
				<div class="tng__tables">
					<table class="tng__table">
						<tr>
							<th class="tng__cell-title" colspan="2">7 лютого</th>
						</tr>
						<tr>
							<th>17:45 - 18:30</th>
							<td>William Pino</td>
						</tr>
						<tr>
							<th>18:30 - 19:15</th>
							<td>Valerio Colantoni</td>
						</tr>
						<tr>
							<th>19:30 - 20:15</th>
							<td>Evgeny Smagin</td>
						</tr>
						<tr>
							<th>20:15 - 21:00</th>
							<td>Sergey Ryupin</td>
						</tr>
					</table>
					<table class="tng__table">
						<tr>
							<th class="tng__cell-title" colspan="2">8 лютого</th>
						</tr>
						<tr>
							<th>17:45 - 18:30</th>
							<td>Allan Fletcher</td>
						</tr>
						<tr>
							<th>18:30 - 19:15</th>
							<td>Slava Kryklyvyy</td>
						</tr>
						<tr>
							<th>19:30 - 20:15</th>
							<td>Cristopher Short</td>
						</tr>
						<tr>
							<th>20:15 - 21:00</th>
							<td>Cristopher Hawkins</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="additional">
				<div class="additional__inner">
					<div class="additional__unit">
						<h4 class="additional__title">Тренувальні збори та уроки 7-8 лютого</h4>
						<!-- <span>Sergey Ryupin, Vibeke Toft, Maurizio Vescovo, Christopher Short, Warren Boyce,
						Lyn Marriner, Hannes Emrich, Simona Fancello, Evgeny Ryupin, Klaus Kongsdal</span> -->
						<!-- <h4 class="additional__title">Лектори латини</h4>
						<span>Sergey Ryupin, Vibeke Toft, Klaus Kongsdal, Maurizio Vescovo</span>
						<h4 class="additional__title">Лектори стандарт</h4>
						<span>Simona Fancello, Christopher Short, Warren Boyce, Lyn Marriner</span> -->
						<span>Sergey Ryupin (Russia), Evgeny Smagin (Russia), Cristopher Hawkins (England), Christopher Short (England), William Pino (Italia), Valerio Colantoni (Italia), Allan Fletcher (England), Slava Kryklyvyy (Ukraine)</span>
						
						<h4 class="additional__title">Лектори латини</h4>
						<span>Sergey Ryupin(Russia), Allan Fletcher(England), Evgeny Smagin (Russia), Slava Kryklyvyy (Ukraine)</span>

						<h4 class="additional__title">Лектори стандарт</h4>
						<span>Valerio Colantoni (Italia), William Pino (Italia), Christopher Short(England), Cristopher Hawkins(England)</span>
					</div>
					<div class="additional__unit">
						<h4 class="additional__title">Бронювання уроків</h4>
						<p>
							<span>Юля</span>
							<a href="tel:+380637026994">+38 (063) 702-69-94</a>
						</p>
						<p>
							<span>Катерина</span>
							<a href="tel:+380504747274">+38 (050) 474-72-74</a>
							<a href="mailto:katyamail@ukr.net">katyamail@ukr.net</a>
						</p>
						<!-- <p><em>Володарі карток WDCAL Ukraine - матимуть знижку 30%.</em></p>
						<p><em>Безкоштовні лекції для пар категорії професіонали з ліцензією WDC Competitor
						Ukraine.</em></p> -->

						<h4 class="additional__title">Групові лекції </h4>
						<span>7-8 лютого 4 години/день (всього 8) - 4 латина та 4 стандарт</span>
					
						<h4 class="additional__title">Збори проходитимуть за адресою</h4>
						<span>м. Київ, вул. Вадима Гетьмана 6, ТРЦ "Космополіт", 4 поверх, зал "Хичкок"</span>
						
					</div>
				</div>
			</div>
			
		</section>
	</main>
	
	<?php include 'components/footer.php' ?>

</body>
</html>






