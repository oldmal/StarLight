<?php include 'components/head.php' ?>
	<title>Місце проведення</title>
</head>
<body class="page-wrap gallery-page">

	<?php include 'components/header.php' ?>
	
	<div class="hero-2">
		<div class="hero-2__unit">
			<h1 class="hero-2__title"><img class="hero-2__title-img" alt="StarLight" src="../img/content/main-text.svg"></h1>
		</div>
		<div class="hero-2__unit">
			<h2 class="hero-2__subtitle"><img class="hero-2__subtitle-img" alt="Grand Prix Cup" src="../img/content/secondary-text.svg"></h2>
		</div>
	</div>

	<main class="main"> 

		<section class="gal gal--loading">
			<div class="gal__btns">
				<h3 class="gal__title">Галерея</h3>
				<button type="button" class="gal__btn gal__btn--active" id="gal__btn-photo-2016">Фото 2016</button>
				<button type="button" class="gal__btn" id="gal__btn-video-2016">Відео 2016</button>
				<button type="button" class="gal__btn" id="gal__btn-photo-2017">Фото 2017</button>
				<button type="button" class="gal__btn" id="gal__btn-video-2017">Відео 2017</button>
				<button type="button" class="gal__btn" id="gal__btn-photo-2018">Фото 2018</button>
				<button type="button" class="gal__btn" id="gal__btn-video-2018">Відео 2018</button>
				<button type="button" class="gal__btn" id="gal__btn-photo-2019">Фото 2019</button>
				<button type="button" class="gal__btn" id="gal__btn-video-2019">Відео 2019</button>
				<p class="gal__subtitle" id="galSubtitle">ФОТО 2016</p>
			</div>
			<img class="gallery__loader" src="../img/content/loader.svg" alt="loader">

			<div class="gall__units gallery">

				<div class="gall__unit grid" id="gallPhoto2016">
					<?php $i = '1'; ?>
					<?php $k = '1'; ?>
					<!-- set amount of photo for gallery -->
					<?php for ($gal2016Amount = 1; $gal2016Amount <= 73; $gal2016Amount++) { ?>
						<a class="grid-unit" data-fancybox="gal2016" href="../img/photo-2016/gal-<?=$k++;?>.jpg?v=<?=$ver?>"><img class="grid-item" src="../img/photo-2016/gal-<?=$i++;?>.jpg?v=<?=$ver?>" alt="" /></a>
					<?php }; ?>
				</div>

				<div class="gall__unit grid" id="gallPhoto2017">
					<?php $j = '1'; ?>
					<?php $l = '1'; ?>
					<!-- set amount of photo for gallery current 145-->
					<?php for ($gal2017Amount = 1; $gal2017Amount <= 145; $gal2017Amount++) { ?>
						<a class="grid-unit" data-fancybox="gal2017" href="../img/photo-2017/gal-<?=$l++;?>.jpg?v=<?=$ver?>"><img class="grid-item" src="../img/photo-2017/gal-<?=$j++;?>.jpg?v=<?=$ver?>" alt="" /></a>
					<?php }; ?>
				</div>

				<div class="gall__unit grid" id="gallPhoto2018">
					<?php $m = '1'; ?>
					<?php $n = '1'; ?>
					<!-- set amount of photo for gallery current 114-->
					<?php for ($gal2018Amount = 1; $gal2018Amount <= 114; $gal2018Amount++) { ?>
						<a class="grid-unit" data-fancybox="gal2018" href="../img/photo-2018/gal-<?=$n++;?>.jpg?v=<?=$ver?>"><img class="grid-item" src="../img/photo-2018/gal-<?=$m++;?>.jpg?v=<?=$ver?>" alt="" /></a>
					<?php }; ?>
				</div>

				<div class="gall__unit grid" id="gallPhoto2019">
					<?php $q = '1'; ?>
					<?php $w = '1'; ?>
					<!-- set amount of photo for gallery current 114-->
					<?php for ($gal2019Amount = 1; $gal2019Amount <= 113; $gal2019Amount++) { ?>
						<a class="grid-unit" data-fancybox="gal2019" href="../img/photo-2019/gal-<?=$w++;?>.jpg?v=<?=$ver?>"><img class="grid-item" src="../img/photo-2019/gal-<?=$q++;?>.jpg?v=<?=$ver?>" alt="" /></a>
					<?php }; ?>
				</div>

				<div class="gall__unit" id="gallVideo2016">
					<iframe class="gal__video-item" src="https://www.youtube.com/embed/e97Ps9wbIdo?list=PLQ0UiL9QsLmbyrj1rDeJzwarGvsALwxrf" allowfullscreen></iframe>
				</div>
				<div class="gall__unit" id="gallVideo2017">
					<iframe class="gal__video-item" src="https://www.youtube.com/embed/9BeXK9Xr_6Y?list=PLQ0UiL9QsLmZu8BdS7AMwgFyv0EVmiOmb" allowfullscreen></iframe>
				</div>
				<div class="gall__unit" id="gallVideo2018">
					<iframe class="gal__video-item" src="https://www.youtube.com/embed/shtoOnTrqMM?list=PLQ0UiL9QsLmaSKcdbhksExQ6r8pErFETK" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				</div>
				<div class="gall__unit" id="gallVideo2019">
					<iframe class="gal__video-item" src="https://www.youtube.com/embed/FUvIz-az4_o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
			
		</section>
	</main>
	
	<?php include 'components/footer.php' ?>

</body>
</html>






