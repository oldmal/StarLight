<!-- START HEADER -->
<header class="header">
	<nav class="nav">
		<ul class="nav__block">
			<li class="nav__item"><a class="nav__item-link" href="index.php">Змагання</a></li>
			<!-- <li class="nav__item"><a class="nav__item-link" target="_blank" href="https://flymark.com.ua/Competition/Details/951">Реєстрація</a></li> -->
			<!-- <li class="nav__item"><a class="nav__item-link" href="schedule.php">Розклад та Entry form</a></li> -->
			<!-- <li class="nav__item"><a class="nav__item-link" href="judges.php">Судді</a></li> -->
			<!-- <li class="nav__item"><a class="nav__item-link" href="training.php">Тренувальні збори</a></li> -->
			<li class="nav__item"><a class="nav__item-link" href="location.php">Місце</a></li>
			<li class="nav__item"><a class="nav__item-link" href="contact.php">Контакти</a></li>
			<li class="nav__item"><a class="nav__item-link" href="gallery.php">Галерея</a></li>
		</ul>
	</nav>
	<div class="lang">
		<button type="button" class="lang__item lang__item--active" data-lang="uk">UKR</button>/
		<button type="button" class="lang__item" data-lang="en">EN</button>
	</div>

</header>
<!-- END HEADER -->