<?php include 'components/head.php' ?>
	<title>StarLight</title>
</head>
<body class="page-wrap page-wrap--hidden index-page">
	
	<?php include 'components/header.php' ?>

	<main class="main">

		<div class="hero">
			<h1 class="hero__title"><img class="hero__title-img" alt="StarLight" src="../img/content/main-text.svg"></h1>
			<h2 class="hero__subtitle"><img class="hero__subtitle-img" alt="Grand Prix Cup" src="../img/content/secondary-text.svg"></h2>
		</div>
		
		<section class="compet" id="compet">
			<div class="compet__content">
				<h3 class="compet__title">Змагання</h3>
				<p class="compet__text-sidebar">8 - 9 ЛЮТОГО</p>
				<p class="compet__text">8-9 лютого 2020 року у Києві відбудеться подія світового рівня, учасником якого Ви можете стати – Міжнародні Танцювальні змагання StarLight "Grand Prix" Cup 2020, який вже увійшов у число кращих змагань України та зарекомендував себе, як надзвичайно красива, елегантна та водночас дуже енергетично насичена подія.</p>
				<!-- <a class="compet__link" target="_blank" href="https://flymark.com.ua/Competition/Details/951">Реєстрація</a> -->
			</div>
			<div class="compet__slides fotorama" data-autoplay="4000" data-stopautoplayontouch="false" data-loop="true" data-fit="cover" >
				<?php for ($competNum = 1; $competNum <= 12; $competNum++) { ?>
					<a href="../img/compet/compet-slide-<?=$competNum?>.jpg?v=<?=$ver?>"></a>
				<?php }; ?>
			</div>
			<!-- <div class="compet__slides">
				<img src="../img/compet/compet-slide-8.jpg?v=<?=$ver?>" alt="" style="object-fit: cover; display: block; height: 100%; width: 100%;">
			</div> -->
		</section>
		<section class="schedule" id="schedule">
			<h3 class="schedule__title">Розклад</h3>
			<div class="schedule__units">
				<div class="schedule__unit">
					<div class="schedule__date">
						<p class="schedule__day">8</p>
						<p class="schedule__month">лютого</p>
					</div>
					<div class="schedule__items">
						<div class="schedule__item">
							<p class="schedule__item-time">9:00</p>
							<p class="schedule__item-title">Перше відділення</p>
						</div>
						<div class="schedule__item">
							<p class="schedule__item-time">13:00</p>
							<p class="schedule__item-title">Друге відділення</p>
						</div>
						<div class="schedule__item">
							<p class="schedule__item-time">18:00</p>
							<p class="schedule__item-title">Треттє відділення</p>
						</div>
					</div>
				</div>

				<div class="schedule__unit">
					<div class="schedule__date">
						<p class="schedule__day">9</p>
						<p class="schedule__month">лютого</p>
					</div>
					<div class="schedule__items">
						<div class="schedule__item">
							<p class="schedule__item-time">9:00</p>
							<p class="schedule__item-title">Перше відділення</p>
						</div>
						<div class="schedule__item">
							<p class="schedule__item-time">13:00</p>
							<p class="schedule__item-title">Друге відділення</p>
						</div>
						<div class="schedule__item">
							<p class="schedule__item-time">18:00</p>
							<p class="schedule__item-title">Треттє відділення</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="tng" id="tng">
			<h3 class="tng__title">Тренувальні збори 6-7 лютого</h3>
			<p class="tng__text">Тренувальні збори з найкращими тренерами світу. Групові та приватні уроки.</p>
			<div class="tng__text">
				<span>Бронь уроків та інформація за тел. </span>
				<a class="tng__phone" href="tel:+380637026994">+38 (063) 702-69-94</a>
				<span>(Юля)</span><br>або
				<a class="tng__phone" href="tel:+380504747274">+38 (050) 474-72-74</a>
				<span>(Катерина)</span>
				<br>
				<a class="tng__email" href="mailto:katyamail@ukr.net">katyamail@ukr.net</a>
			</div>
			<iframe class="tng__video" src="https://www.youtube.com/embed/tRvIbwfTb-c?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		</section>
		<section class="partners">
			<h3 class="partners__title">Партнери</h3>
			<div class="partners__unit">
				<div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-1.png?v=<?=$ver?>" alt="">
				</div>
				<div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-2.png?v=<?=$ver?>" alt="">
				</div>
				<div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-3.png?v=<?=$ver?>" alt="">
				</div>
				<div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-4.png?v=<?=$ver?>" alt="">
				</div>
			</div>
			<div class="partners__unit">
				<div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-5.png?v=<?=$ver?>" alt="">
				</div>
				<!-- <div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-6.png?v=<?=$ver?>" alt="">
				</div> -->
				<div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-7.png?v=<?=$ver?>" alt="">
				</div>
				<!-- <div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-8.png?v=<?=$ver?>" alt="">
				</div> -->
				<div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-11.png?v=<?=$ver?>" alt="">
				</div>
			</div>
			<div class="partners__unit">
				<div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-9.png?v=<?=$ver?>" alt="">
				</div>
				<div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-10.png?v=<?=$ver?>" alt="">
				</div>
				
			</div>
		</section>
	</main>
	
	<?php include 'components/footer.php' ?>

</body>
</html>






