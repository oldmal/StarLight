<?php include 'components/head.php' ?>
	<title>StarLight Judges</title>
</head>
<body class="page-wrap judges-page">
	
	<?php include 'components/header.php' ?>
	
	<div class="hero-2">
		<div class="hero-2__unit">
			<h1 class="hero-2__title"><img class="hero-2__title-img" alt="StarLight" src="../img/content/main-text.svg"></h1>
		</div>
		<div class="hero-2__unit">
			<h2 class="hero-2__subtitle"><img class="hero-2__subtitle-img" alt="Grand Prix Cup" src="../img/content/secondary-text.svg"></h2>
		</div>
	</div>

	<main class="main">
		<section class="judges">
			
			<div class="judges__unit">
				<h3 class="judges__title">Judges</h3>
				<div>	
					<ol>
						<li>Robert Bellinger (England)</li>
						<li>Slava Kryklyvyy (Ukraine)</li>
						<li>Andrej Mosejcuk (Poland)</li>
						<li>Sergey Ryupin (Russia)</li>
						<li>Christopher Short (England)</li>
						<li>William Pino (Italia)</li>
						<li>Valerio Colantoni (Italia)</li>
						<li>Evgeny Smagin (Russia)</li>
						<li>Cristopher Hawkins (England)</li>
						<li>Allan Fletcher (England)</li>
						<li>Ruslan Aidaev (Russia)</li>
						<li>Isao Wolvekamp (Holland)</li>
						<li>Robin Short (England)</li>
					</ol>
					<p>And Ukrainian judges</p>
				</div>
			</div>
		</section>
	</main>
	
	<?php include 'components/footer.php' ?>

</body>
</html>






