<?php include 'components/head.php' ?>
	<title>StarLight Time table</title>
</head>
<body class="page-wrap schedule-page">
	
	<?php include 'components/header.php' ?>
	
	<div class="hero-2">
		<div class="hero-2__unit">
			<h1 class="hero-2__title"><img class="hero-2__title-img" alt="StarLight" src="../img/content/main-text.svg"></h1>
		</div>
		<div class="hero-2__unit">
			<h2 class="hero-2__subtitle"><img class="hero-2__subtitle-img" alt="Grand Prix Cup" src="../img/content/secondary-text.svg"></h2>
		</div>
	</div>

	<main class="main">
		<section class="schedule" id="schedule">
			<h3 class="schedule__title">Time table</h3>
			<div class="schedule__units">
				<div class="schedule__unit">
					<div class="schedule__date">
						<p class="schedule__day">9</p>
						<p class="schedule__month">February</p>
					</div>
					<div class="schedule__items">
						<div class="schedule__item">
							<p class="schedule__item-time">9:00</p>
							<h3 class="schedule__item-title">First session</h3>
							<table class="schedule__item-table">
								<tr>
									<th title="Group number">№</th>
									<th>Category</th>
									<th>Dance</th>
								</tr>
								<tr>
									<td>5</td>
									<td>Youth 1+2 RS</td>
									<td>St</td>
								</tr>
								<tr>
									<td>8</td>
									<td>Juniors 2 C</td>
									<td>Lat</th>
								</tr>
								<tr>
									<td>11</td>
									<td>Juniors 1 C</td>
									<td>St</td>
								</tr>
								<tr>
									<td>13</td>
									<td>Juniors 1 E</td>
									<td>St</td>
								</tr>
								<tr>
									<td>16</td>
									<td>Juveniles 2 E</td>
									<td>St</td>
								</tr>
								<tr>
									<td>19</td>
									<td>Juveniles 1 E</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>20</td>
									<td>Juveniles 1 H</td>
									<td>W,Q,Ch,J</td>
								</tr>
								<tr>
									<td>21</td>
									<td>Juveniles 1 School</td>
									<td>W,Ch,J</td>
								</tr>
								<tr>
									<td>24</td>
									<td>Kids Debute (solo) under 8</td>
									<td>W,Ch</td>
								</tr>
							</table>
						</div>
						<div class="schedule__item">
							<p class="schedule__item-time">13:00</p>
							<h3 class="schedule__item-title">Second session</h3>
							<table class="schedule__item-table">
								<tr>
									<th title="Group number">№</th>
									<th>Category</th>
									<th>Dance</th>
								</tr>
								<tr>
									<td>2</td>
									<td>Adults RS</td>
									<td>St</td>
								</tr>
								<tr>
									<td>7</td>
									<td>Juniors 2 RS</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>10</td>
									<td>Juniors 1 RS</td>
									<td>St</td>
								</tr>
								<tr>
									<td>12</td>
									<td>Juniors 1 D</td>
									<td>St</td>
								</tr>
								<tr>
									<td>16</td>
									<td>Juveniles 2 E</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>19</td>
									<td>Juveniles 1 E</td>
									<td>St</td>
								</tr>
								<tr>
									<td>26</td>
									<td>BIG Dance Pro-Am Cup (Scolarship) A(17-35)</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>27</td>
									<td>BIG Dance Pro-Am Cup (Scolarship) B(36+)</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>28</td>
									<td>BIG Dance Pro-Am Cup (Single dance) A(17-35)</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>29</td>
									<td>BIG Dance Pro-Am Cup (Single dance) B(36+)</td>
									<td>Lat</td>
								</tr>
							</table>
						</div>
						<div class="schedule__item">
							<p class="schedule__item-time">18:00</p>
							<h3 class="schedule__item-title">Third session</h3>
							<table class="schedule__item-table">
								<tr>
									<th title="Group number">№</th>
									<th>Category</th>
									<th>Dance</th>
								</tr>
								<tr>
									<td>1</td>
									<td>Adults</td>
									<td>St</td>
								</tr>
								<tr>
									<td>3</td>
									<td>Youth 2</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>4</td>
									<td>Youth 1</td>
									<td>St</td>
								</tr>
								<tr>
									<td>6</td>
									<td>Juniors 2</td>
									<td>Lat</td>
								</tr>
								<tr>
									<td>9</td>
									<td>Juniors 1</td>
									<td>St</td>
								</tr>
								<tr>
									<td>14</td>
									<td>Juveniles 1+2</td>
									<td>Lat</td>
								</tr>
							</table>
						</div>
					</div>
				</div>

				<div class="schedule__unit">
					<div class="schedule__date">
						<p class="schedule__day">10</p>
						<p class="schedule__month">February</p>
					</div>
						<div class="schedule__items">
							<div class="schedule__item">
								<p class="schedule__item-time">9:00</p>
								<h3 class="schedule__item-title">First session</h3>
								<table class="schedule__item-table">
									<tr>
										<th title="Group number">№</th>
										<th>Category</th>
										<th>Dance</th>
									</tr>
									<tr>
										<td>5</td>
										<td>Youth 1+2 RS</td>
										<td>Lat</td>
									</tr>
									<tr>
										<td>11</td>
										<td>Juniors 1 C</td>
										<td>Lat</td>
									</tr>
									<tr>
										<td>13</td>
										<td>Juniors 1 E</td>
										<td>Lat</td>
									</tr>
									<tr>
										<td>15</td>
										<td>Juveniles 2 D</td>
										<td>St</td>
									</tr>
									<tr>
										<td>17</td>
										<td>Juveniles 2 H</td>
										<td>W,Q,Ch,J</td>
									</tr>
									<tr>
										<td>18</td>
										<td>Juveniles 2 School</td>
										<td>W,Ch,J</td>
									</tr>
									<tr>
										<td>22</td>
										<td>Juveniles 1(solo) School</td>
										<td>W,Ch</td>
									</tr>
									<tr>
										<td>23</td>
										<td>Juveniles 2 (solo) Н</td>
										<td>W,Q,Ch,J</td>
									</tr>
									<tr>
										<td>25</td>
										<td>Kids Debute under 8</td>
										<td>W,Ch</td>
									</tr>
								</table>
							</div>
							<div class="schedule__item">
								<p class="schedule__item-time">13:00</p>
								<h3 class="schedule__item-title">Second session</h3>
								<table class="schedule__item-table">
									<tr>
										<th title="Group number">№</th>
										<th>Category</th>
										<th>Dance</th>
									</tr>
									<tr>
										<td>2</td>
										<td>Adults RS</td>
										<td>Lat</td>
									</tr>
									<tr>
										<td>7</td>
										<td>Juniors 2 RS</td>
										<td>St</td>
									</tr>
									<tr>
										<td>8</td>
										<td>Juniors 2 C</td>
										<td>St</td>
									</tr>
									<tr>
										<td>10</td>
										<td>Juniors 1 RS</td>
										<td>Lat</td>
									</tr>
									<tr>
										<td>12</td>
										<td>Juniors 1 D</td>
										<td>Lat</td>
									</tr>
									<tr>
										<td>15</td>
										<td>Juveniles 2 D</td>
										<td>Lat</td>
									</tr>
									<tr>
										<td>26</td>
										<td>BIG Dance Pro-Am Cup (Scolarship) A(17-35)</td>
										<td>St</td>
									</tr>
									<tr>
										<td>27</td>
										<td>BIG Dance Pro-Am Cup (Scolarship) B(36+)</td>
										<td>St</td>
									</tr>
									<tr>
										<td>28</td>
										<td>BIG Dance Pro-Am Cup (Single dance) A(17-35)</td>
										<td>St</td>
									</tr>
									<tr>
										<td>29</td>
										<td>BIG Dance Pro-Am Cup (Single dance) B(36+)</td>
										<td>St</td>
									</tr>
								</table>
							</div>
							<div class="schedule__item">
								<p class="schedule__item-time">18:00</p>
								<h3 class="schedule__item-title">Third session</h3>
								<table class="schedule__item-table">
									<tr>
										<th title="Group number">№</th>
										<th>Category</th>
										<th>Dance</th>
									</tr>
									<tr>
										<td>1</td>
										<td>Adults</td>
										<td>Lat</td>
									</tr>
									<tr>
										<td>3</td>
										<td>Youth 2</td>
										<td>St</td>
									</tr>
									<tr>
										<td>4</td>
										<td>Youth 1</td>
										<td>Lat</td>
									</tr>
									<tr>
										<td>6</td>
										<td>Juniors 2</td>
										<td>St</td>
									</tr>
									<tr>
										<td>9</td>
										<td>Juniors 1</td>
										<td>Lat</td>
									</tr>
									<tr>
										<td>14</td>
										<td>Juveniles 1+2</td>
										<td>St</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="schedule__gal" data-stopautoplayontouch="false" data-autoplay="true" data-loop="true" data-fit="cover">
				<?php for ($scheduleNum = 1; $scheduleNum <= 5; $scheduleNum++) { ?>
					<a href="../img/schedule/sch-slide-<?=$scheduleNum?>.jpg?v=<?=$ver?>"></a>
				<?php }; ?>
			</div>
		</section>
		<section class="fee">
			<h3 class="schedule__subtitle">Fee</h3>
			<table class="fee__table">
				<tr>
					<th>Group 1-11, 14</th>
					<td>25 Euro per person</td>
				</tr>
				<tr>
					<th>Group 12, 13, 15, 16, 19</th>
					<td>350 UAH per person</td>
				</tr>
				<tr>
					<th>Group 17, 20, 23</th>
					<td>300 UAH per person</td>
				</tr>
				<tr>
					<th>Group 18, 21, 22, 24, 25</th>
					<td>250 UAH per person</td>
				</tr>
				<tr>
					<th>Group 26-29</th>
					<td>650 UAH per person</td>
				</tr>
			</table>
			
		</section>

		<section class="additional">
			<div class="additional__inner">
				<div>
					<h4 class="additional__title">ProAm information</h4>
					<span>Valeria</span>
					<a href="tel:+380932098686">+38 (093) 209-86-86</a>
					<a href="mailto:BIGProAmDance@gmail.com">BIGProAmDance@gmail.com</a>
				</div>
				<div>
					<h4 class="additional__title">Entry tickets</h4>
					<!-- <p>200 UAH for the first session, 250 UAH for 2nd and 3rd sessions.</p> -->
					<p>1 session - 100 UAH. / place at the table + ticket - 200 UAH.</p>
					<p>2 session - 150 UAH. / place at the table + ticket - 250 UAH.</p>
					<p>3 session - 200 UAH. / place at the table + ticket - 400 UAH.</p>
				</div>
				<div>
					<h4 class="additional__title">Booking table</h4>
					<span>Julia</span>
					<a href="tel:+380991337729">+38 (099) 133-77-29</a>
				</div>
				<div>
					<h4 class="additional__title">Booking lessons</h4>
					<span>Julia</span>
					<a href="tel:+380637026994">+38 (063) 702-69-94</a>
				</div>
				<div>
					<h4 class="additional__title">Entries</h4>
					<span>Closing date of Entries - 07.02.2019</span>
					<a href="https://flymark.com.ua/Competition/Details/951">GO TO REGISTRATION</a>
					<p><em>If you not make an online registration, you will pay a double price of it. Pre-registration is a necessity. Registration for couples from Kyiv will run on Friday the 8th of February from 11:00 am till 8:00pm in <br>Kyiv, 6 Vadyma Hetmana St., shopping mall "Cosmopolite", 4th floor, "Hitchcock" hall</em></p>
				</div>
			</div>
		</section>

		<!-- <section class="prize">
			<h3 class="schedule__subtitle">Prize money</h3>
			<table class="prize__table">
				<tr>
					<th>Category</th>
					<th>1</th>
					<th>2</th>
					<th>3</th>
					<th>4</th>
					<th>5</th>
					<th>6</th>
				</tr>
				<tr>
					<th>Adults</th>
					<td>4000</td>
					<td>3000</td>
					<td>2000</td>
					<td>1000</td>
					<td>1000</td>
					<td>1000</td>
				</tr>
				<tr>
					<th>Youth 2</th>
					<td>3000</td>
					<td>2000</td>
					<td>1500</td>
					<td>700</td>
					<td>600</td>
					<td>600</td>
				</tr>
				<tr>
					<th>Youth 1</th>
					<td>3000</td>
					<td>2000</td>
					<td>1500</td>
					<td>700</td>
					<td>600</td>
					<td>600</td>
				</tr>
				<tr>
					<th>Juniors 2</th>
					<td>P</td>
					<td>P</td>
					<td>P</td>
					<td>P</td>
					<td>P</td>
					<td>P</td>
				</tr>
				<tr>
					<th>Juniors 1</th>
					<td>P</td>
					<td>P</td>
					<td>P</td>
					<td>P</td>
					<td>P</td>
					<td>P</td>
				</tr>
				<tr>
					<th>Juveniles 2</th>
					<td>P</td>
					<td>P</td>
					<td>P</td>
					<td>P</td>
					<td>P</td>
					<td>P</td>
				</tr>
				<tr>
					<th>Juveniles 1</th>
					<td>P</td>
					<td>P</td>
					<td>P</td>
					<td>P</td>
					<td>P</td>
					<td>P</td>
				</tr>
				<tr>
					<td colspan="7">P – prizes from “Grand Prix”</td>
				</tr>
			</table>
		</section> -->
		
		<section class="additional rules">
			<div class="additional__inner">
				<h4 class="additional__title">Rules</h4>
				<span>According to WDC</span>

				<h4 class="additional__title">Organizers</h4>
				<div>
					<span>Roman Myrkin</span>
					<a href="tel:+380503345487">+38 (050) 334-54-87‬</a>‎
					<a href="mailto:wdcalukraine@ukr.net">wdcalukraine@ukr.net</a>
				</div>
				<div>
					<span>Maksym Bulanyi</span>
					<a href="tel:+380504730066">+38 (050) 473-00-66</a>‎
				</div>
			</div>
		</section>
	</main>
	
	<?php include 'components/footer.php' ?>

</body>
</html>






