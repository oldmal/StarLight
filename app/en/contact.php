<?php include 'components/head.php' ?>
	<title>StarLight Contact</title>
</head>
<body class="page-wrap contact-page">
	
	<?php include 'components/header.php' ?>
	
	<div class="hero-2">
		<div class="hero-2__unit">
			<h1 class="hero-2__title"><img class="hero-2__title-img" alt="StarLight" src="../img/content/main-text.svg"></h1>
		</div>
		<div class="hero-2__unit">
			<h2 class="hero-2__subtitle"><img class="hero-2__subtitle-img" alt="Grand Prix Cup" src="../img/content/secondary-text.svg"></h2>
		</div>
	</div>

	<main class="main">
		<section class="contact">
			<h3 class="contact__title">Contact</h3>
			<div class="contact__unit">
				<div class="contact__unit-title">Organizers:</div>
				<div class="contact__item">
					<p class="contact__item-name">Roman Myrkin</p>
					<a class="contact__item-phone" href="tel:+380503345487">+38 (050) 334-54-87‬</a>‎
					<a class="contact__item-email" href="mailto:romnatik@ukr.net">romnatik@ukr.net</a>
					<a class="contact__item-email" href="mailto:wdcalukraine@ukr.net">wdcalukraine@ukr.net</a>
				</div>
				<div class="contact__item">
					<p>Maksym Bulanyi</p>
					<a href="tel:+380504730066">+38 (050) 473-00-66</a>‎
				</div>
			</div>
		</section>
	</main>
	
	<?php include 'components/footer.php' ?>

</body>
</html>






