<?php include 'components/head.php' ?>
	<title>StarLight Training camp</title>
</head>
<body class="page-wrap training-page">
	
	<?php include 'components/header.php' ?>
	
	<div class="hero-2">
		<div class="hero-2__unit">
			<h1 class="hero-2__title"><img class="hero-2__title-img" alt="StarLight" src="../img/content/main-text.svg"></h1>
		</div>
		<div class="hero-2__unit">
			<h2 class="hero-2__subtitle"><img class="hero-2__subtitle-img" alt="Grand Prix Cup" src="../img/content/secondary-text.svg"></h2>
		</div>
	</div>

	<main class="main">
		<section class="tng" id="tng">
			<h3 class="tng__title">Training camp 7-8 February</h3>
				<p class="tng__text">Training camp with the best world coaches. Group and privat classes.</p>
				<div class="tng__units">
					<div class="tng__unit">
						<!-- Valerio Colantoni -->
						<div class="tng__item">
							<img class="tng__item-img" src="../img/coaches/ryupin.jpg?v=<?=$ver?>" alt="Sergey Ryupin">
							<p class="tng__item-text">Sergey Ryupin</p>
						</div>
						<div class="tng__item">
							<img class="tng__item-img" src="../img/coaches/pino.jpg?v=<?=$ver?>" alt="William Pino">
							<p class="tng__item-text">William Pino</p>
						</div>
						<div class="tng__item">
							<img class="tng__item-img" src="../img/coaches/fletcher.jpg?v=<?=$ver?>" alt="Allan Fletcher">
							<p class="tng__item-text">Allan Fletcher</p>
						</div>
						
						<!-- <div class="tng__item">
							<img class="tng__item-img" src="../img/coaches/toft.jpg?v=<?=$ver?>" alt="Vibeke Toft">
							<p class="tng__item-text">Vibeke Toft</p>
						</div>
						<div class="tng__item">
							<img class="tng__item-img" src="../img/coaches/boyce.jpg?v=<?=$ver?>" alt="Warren Boyce">
							<p class="tng__item-text">Warren Boyce</p>
						</div> -->
					</div>
					<div class="tng__unit">
						<div class="tng__item">
							<img class="tng__item-img" src="../img/coaches/short.jpg?v=<?=$ver?>" alt="Christopher Short">
							<p class="tng__item-text">Christopher Short</p>
						</div>
						<div class="tng__item">
							<img class="tng__item-img" src="../img/coaches/hawkins.jpg?v=<?=$ver?>" alt="Cristopher Hawkins">
							<p class="tng__item-text">Cristopher Hawkins</p>
						</div>
						<div class="tng__item">
							<img class="tng__item-img" src="../img/coaches/kryklyvyy.jpg?v=<?=$ver?>" alt="Slava Kryklyvyy">
							<p class="tng__item-text">Slava Kryklyvyy</p>
						</div>
						
						<!-- <div class="tng__item">
							<img class="tng__item-img" src="../img/coaches/fancello.jpg?v=<?=$ver?>" alt="Simona Fancello">
							<p class="tng__item-text">Simona Fancello</p>
						</div>
						<div class="tng__item">
							<img class="tng__item-img" src="../img/coaches/marriner.jpg?v=<?=$ver?>" alt="Lyn Marriner">
							<p class="tng__item-text">Lyn Marriner</p>
						</div> -->
					</div>
					<div class="tng__unit">
						<div class="tng__item">
							<img class="tng__item-img" src="../img/coaches/colantoni.jpg?v=<?=$ver?>" alt="Valerio Colantoni">
							<p class="tng__item-text">Valerio Colantoni</p>
						</div>
						<div class="tng__item">
							<img class="tng__item-img" src="../img/coaches/smagin.jpg?v=<?=$ver?>" alt="Evgeny Smagin">
							<p class="tng__item-text">Evgeny Smagin</p>
						</div>

						<!-- <div class="tng__item">
							<img class="tng__item-img" src="../img/coaches/emrich.jpg?v=<?=$ver?>" alt="Hannes Emrich">
							<p class="tng__item-text">Hannes Emrich</p>
						</div>
						<div class="tng__item">
							<img class="tng__item-img" src="../img/coaches/kongsdal.jpg?v=<?=$ver?>" alt="Klaus Kongsdal">
							<p class="tng__item-text">Klaus Kongsdal</p>
						</div>
						<div class="tng__item">
							<img class="tng__item-img" src="../img/coaches/vescovo.jpg?v=<?=$ver?>" alt="Maurizio Vescovo">
							<p class="tng__item-text">Maurizio Vescovo</p>
						</div> -->
					</div>
				</div>
				<div class="tng__text">
					<span>Information and booking lessons: </span> 
					<a class="tng__phone" href="tel:+380637026994">+38 (063) 702-69-94</a>
					<span>(Julia)</span><br>or
					<a class="tng__phone" href="tel:+380504747274">+38 (050) 474-72-74</a>
					<span>(Kateryna)</span>
					<br>
					<a class="tng__email" href="mailto:katyamail@ukr.net">katyamail@ukr.net</a>
				</div>
			<!-- <iframe class="tng__video" src="https://www.youtube.com/embed/ptg82f53yOs?rel=0" allowfullscreen></iframe> -->
			<iframe class="tng__video" src="https://www.youtube.com/embed/tRvIbwfTb-c?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

			<!-- <div class="tng__gallery tng__gallery--hidden grid" id="tngGallery">
				<img class="grid-item" src="../img/tng/slide-1.jpg?v=<?=$ver?>" alt="">	
				<img class="grid-item" src="../img/tng/slide-2.jpg?v=<?=$ver?>" alt="">	
				<img class="grid-item" src="../img/tng/slide-3.jpg?v=<?=$ver?>" alt="">	
				<img class="grid-item" src="../img/tng/slide-4.jpg?v=<?=$ver?>" alt="">	
				<img class="grid-item" src="../img/tng/slide-5.jpg?v=<?=$ver?>" alt="">	
				<img class="grid-item" src="../img/tng/slide-6.jpg?v=<?=$ver?>" alt="">
			</div> -->
			
			<div class="tng__schedule">
				<h3 class="schedule__subtitle">Timetable of training camp</h3>
				<div class="tng__tables">
					<table class="tng__table">
						<tr>
							<th class="tng__cell-title" colspan="2">7th of February</th>
						</tr>
						<tr>
							<th>17:45 - 18:30</th>
							<td>William Pino</td>
						</tr>
						<tr>
							<th>18:30 - 19:15</th>
							<td>Valerio Colantoni</td>
						</tr>
						<tr>
							<th>19:30 - 20:15</th>
							<td>Evgeny Smagin</td>
						</tr>
						<tr>
							<th>20:15 - 21:00</th>
							<td>Sergey Ryupin</td>
						</tr>
					</table>
					<table class="tng__table">
						<tr>
							<th class="tng__cell-title" colspan="2">8th of February</th>
						</tr>
						<tr>
							<th>17:45 - 18:30</th>
							<td>Allan Fletcher</td>
						</tr>
						<tr>
							<th>18:30 - 19:15</th>
							<td>Slava Kryklyvyy</td>
						</tr>
						<tr>
							<th>19:30 - 20:15</th>
							<td>Cristopher Short</td>
						</tr>
						<tr>
							<th>20:15 - 21:00</th>
							<td>Cristopher Hawkins</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="additional">
				<div class="additional__inner">
					<div class="additional__unit">
						<h4 class="additional__title">Training camp and lessons 7-8 of February</h4>
						<span>Sergey Ryupin (Russia), Evgeny Smagin (Russia), Cristopher Hawkins (England), Christopher Short (England), William Pino (Italia), Valerio Colantoni (Italia), Allan Fletcher (England), Slava Kryklyvyy (Ukraine)</span>
						
						<h4 class="additional__title">Latin classes</h4>
						<span>Sergey Ryupin(Russia), Allan Fletcher(England), Evgeny Smagin (Russia), Slava Kryklyvyy (Ukraine)</span>
						
						<h4 class="additional__title">Ballroom classes</h4>
						<span>Valerio Colantoni (Italia), William Pino (Italia), Christopher Short(England), Cristopher Hawkins(England)</span>

					</div>
					<div class="additional__unit">
						<h4 class="additional__title">Booking lessons</h4>
						<p>
							<span>Julia</span>
							<a href="tel:+380637026994">+38 (063) 702-69-94</a>
						</p>
						<p>
							<span>Kateryna</span>
							<a href="tel:+380504747274">+38 (050) 474-72-74</a>
							<a href="mailto:katyamail@ukr.net">katyamail@ukr.net</a>
						</p>
						
						<!-- <p><em>Owners of WDC AL Ukraine cards will have the discount – 30%.</em></p> -->
						<!-- <p><em>Free group classes for professional couples who have the license WDC AL Competitor Ukraine.</em></p> -->

						<h4 class="additional__title">Group classes</h4>
						<span>7-8th of February 4 hours a day for Latin and 4 hours a day for Ballroom (8 hours together)</span>

						<h4 class="additional__title">All lessons will run in</h4>
						<span>Kyiv, 6 Vadyma Hetmana St., shopping mall "Cosmopolite", 4th floor, "Hitchcock" hall</span>

					</div>
				</div>
			</div>
		</section>
	</main>
	
	<?php include 'components/footer.php' ?>

</body>
</html>






