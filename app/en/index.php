<?php include 'components/head.php' ?>
	<title>StarLight</title>
</head>
<body class="page-wrap page-wrap--hidden index-page">
	
	<?php include 'components/header.php' ?>

	<main class="main">

		<div class="hero">
			<h1 class="hero__title"><img class="hero__title-img" alt="StarLight" src="../img/content/main-text.svg"></h1>
			<h2 class="hero__subtitle"><img class="hero__subtitle-img" alt="Grand Prix Cup" src="../img/content/secondary-text.svg"></h2>
		</div>
		<section class="compet" id="compet">
			<div class="compet__content">
				<h3 class="compet__title">Competition</h3>
				<p class="compet__text-sidebar">8 - 9 FEBRUARY</p>
				<p class="compet__text">8-9 February 2020 going to be a world-class event in Kiev, Ukraine – International Dance competition StarLight "Grand Prix" Cup 2020. "StarLight" is one of the best competitions in Ukraine and has established itself as an extremely beautiful, elegant and at the same time very energetically fullness event. Don't miss it!!!</p>
				<!-- <a class="compet__link" target="_blank" href="https://flymark.com.ua/Competition/Details/951">Registration</a> -->
			</div>
			<div class="compet__slides fotorama" data-autoplay="4000" data-stopautoplayontouch="false" data-loop="true" data-fit="cover" >
				<?php for ($competNum = 1; $competNum <= 12; $competNum++) { ?>
					<a href="../img/compet/compet-slide-<?=$competNum?>.jpg?v=<?=$ver?>"></a>
				<?php }; ?>
			</div>
			<!-- <div class="compet__slides">
				<img src="../img/compet/compet-slide-8.jpg?v=<?=$ver?>" alt="" style="object-fit: cover; display: block; height: 100%; width: 100%;">
			</div> -->
		</section>
		<section class="schedule" id="schedule">
			<h3 class="schedule__title">Time table</h3>
			<div class="schedule__units">
				<div class="schedule__unit">
					<div class="schedule__date">
						<p class="schedule__day">8</p>
						<p class="schedule__month">February</p>
					</div>
					<div class="schedule__items">
						<div class="schedule__item">
							<p class="schedule__item-time">9:00</p>
							<p class="schedule__item-title">First session</p>
						</div>
						<div class="schedule__item">
							<p class="schedule__item-time">13:00</p>
							<p class="schedule__item-title">First session</p>
						</div>
						<div class="schedule__item">
							<p class="schedule__item-time">18:00</p>
							<p class="schedule__item-title">Third session</p>
						</div>
					</div>
				</div>

				<div class="schedule__unit">
					<div class="schedule__date">
						<p class="schedule__day">9</p>
						<p class="schedule__month">February</p>
					</div>
					<div class="schedule__items">
						<div class="schedule__item">
							<p class="schedule__item-time">9:00</p>
							<p class="schedule__item-title">First session</p>
						</div>
						<div class="schedule__item">
							<p class="schedule__item-time">13:00</p>
							<p class="schedule__item-title">First session</p>
						</div>
						<div class="schedule__item">
							<p class="schedule__item-time">18:00</p>
							<p class="schedule__item-title">Third session</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="tng" id="tng">
			<h3 class="tng__title">Training camp 6-7 February</h3>
			<p class="tng__text">Training camp with the best world coaches. Group and privat classes.</p>
			<div class="tng__text">
				<span>Information and booking lessons: </span> 
				<a class="tng__phone" href="tel:+380637026994">+38 (063) 702-69-94</a>
				<span>(Julia)</span><br>or
				<a class="tng__phone" href="tel:+380504747274">+38 (050) 474-72-74</a>
				<span>(Kateryna)</span>
				<br>
				<a class="tng__email" href="mailto:katyamail@ukr.net">katyamail@ukr.net</a>
			</div>
			<iframe class="tng__video" src="https://www.youtube.com/embed/tRvIbwfTb-c?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		</section>
		<section class="partners">
			<h3 class="partners__title">Partners</h3>
			<div class="partners__unit">
				<div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-1.png?v=<?=$ver?>" alt="">
				</div>
				<div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-2.png?v=<?=$ver?>" alt="">
				</div>
				<div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-3.png?v=<?=$ver?>" alt="">
				</div>
				<div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-4.png?v=<?=$ver?>" alt="">
				</div>
			</div>
			<div class="partners__unit">
				<div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-5.png?v=<?=$ver?>" alt="">
				</div>
				<!-- <div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-6.png?v=<?=$ver?>" alt="">
				</div> -->
				<div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-7.png?v=<?=$ver?>" alt="">
				</div>
				<!-- <div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-8.png?v=<?=$ver?>" alt="">
				</div> -->
				<div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-11.png?v=<?=$ver?>" alt="">
				</div>
			</div>
			<div class="partners__unit">
				<div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-9.png?v=<?=$ver?>" alt="">
				</div>
				<div class="partners__item">
					<img class="partners__item-img" src="../img/partn/partn-10.png?v=<?=$ver?>" alt="">
				</div>
				
			</div>
		</section>
	</main>
	
	<?php include 'components/footer.php' ?>

</body>
</html>






