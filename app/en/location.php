<?php include 'components/head.php' ?>
	<title>StarLight Place</title>
</head>
<body class="page-wrap location-page">
	
	<?php include 'components/header.php' ?>
	
	<div class="hero-2">
		<div class="hero-2__unit">
			<h1 class="hero-2__title"><img class="hero-2__title-img" alt="StarLight" src="../img/content/main-text.svg"></h1>
		</div>
		<div class="hero-2__unit">
			<h2 class="hero-2__subtitle"><img class="hero-2__subtitle-img" alt="Grand Prix Cup" src="../img/content/secondary-text.svg"></h2>
		</div>
	</div>
	<main class="main">
		<section class="location">
			<h3 class="location__title">Place</h3>
			<p class="location__content">
				Shopping mall "Cosmopolite", 5th floor, "Spielberg" Hall<br>
				Vadyma Hetmana St, 6, Kyiv, 02000, Ukraine<br>
				Metro "Shuliavska"<br>
			</p>
			<div class="location__map" id="googleMap"></div>
		</section>
	</main>

	<?php include 'components/footer.php' ?>
	<script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBD1X6JKXHNxBDkAqnlkaFSRWMe7tg1mM&language=en"></script>
</body>
</html>



