<?php $ver = 26 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Global Site Tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107127107-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments)};
	  gtag('js', new Date());

	  gtag('config', 'UA-107127107-1');
	</script>

	<!-- meta -->
	<meta charset="UTF-8">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="StarLight &quot;Grand Prix&quot; Cup 2019">

	<meta name="keywords" content="starlight, StarLight Grand Prix Cup 2019, старлайт, танцевальные соревнования, кубок по танцам, dance cup, dance competitons, танцевальный мир, танцы, танец, спортивные танцы, спортивно-бальные танцы, бальные танцы, регистрация на турнир, чемпионат по танцам" />
	<meta name="author" content="development - Dmytro Oliinyk oldmal.com">


	<meta property="og:site_name" content="StarLight">
	<meta property="og:type" content="website">
	<meta property="og:url" content="http://www.starlight.net.ua/uk">
	<meta property="og:title" content="StarLight">
	<meta property="og:description" content="StarLight &quot;Grand Prix&quot; Cup 2019">
	<meta property="og:image" content="http://www.starlight.net.ua/img/content/shareStarLight.png">
	<link rel="image_src" href="http://www.starlight.net.ua/img/content/shareStarLight.png">
	<meta property="og:image:width" content="408">
	<meta property="og:image:height" content="599">
	<meta name="twitter:image" content="http://www.starlight.net.ua/img/content/shareStarLight.png">
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:title" content="StarLight">
	<meta name="twitter:description" content="StarLight &quot;Grand Prix&quot; Cup 2019">

	<!-- favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="../apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../favicon-16x16.png">
	<link rel="manifest" href="../manifest.json">
	<link rel="mask-icon" href="../safari-pinned-tab.svg" color="#5bbad5">
	<!-- Сhange the color of header bar and address bar -->
	<!-- Chrome, Firefox OS and Opera -->
	<meta name="theme-color" content="#000000">
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#000000">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-status-bar-style" content="#000000">

	<!-- styles -->
	<link rel="stylesheet" href="../css/lib/normalize-7.0.0.upd.css">
	<link rel="stylesheet" href="../css/lib/fotorama.css">
	<link rel="stylesheet" href="../css/lib/jquery.fancybox.min.css">
	<link rel="stylesheet" href="../css/styles.css?v=<?=$ver?>">
