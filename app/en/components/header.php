<!-- START HEADER -->
<header class="header">
	<nav class="nav">
		<ul class="nav__block">
			<li class="nav__item"><a class="nav__item-link" href="index.php">Competition</a></li>
			<!-- <li class="nav__item"><a class="nav__item-link" target="_blank" href="https://flymark.com.ua/Competition/Details/951">Registration</a></li> -->
			<!-- <li class="nav__item"><a class="nav__item-link" href="schedule.php">Time table and Entry form</a></li> -->
			<!-- <li class="nav__item"><a class="nav__item-link" href="judges.php">Judges</a></li> -->
			<!-- <li class="nav__item"><a class="nav__item-link" href="training.php">Training Camp</a></li> -->
			<li class="nav__item"><a class="nav__item-link" href="location.php">Place</a></li>
			<li class="nav__item"><a class="nav__item-link" href="contact.php">Contact</a></li>
			<li class="nav__item"><a class="nav__item-link" href="gallery.php">Gallery</a></li>
		</ul>
	</nav>
	<div class="lang">
		<button type="button" class="lang__item " data-lang="uk">UKR</button>/
		<button type="button" class="lang__item lang__item--active" data-lang="en">EN</button>
	</div>

</header>
<!-- END HEADER -->