<footer class="footer">
	<div class="footer__contact">
		<div class="footer__title">WAITING FOR YOU <strong>8 - 9 FEBRUARY, 2020</strong></div>
		<!-- <a class="footer__btn" target="_blank" href="https://flymark.com.ua/Competition/Details/951">Registration</a> -->
		<address class="footer__address">
			<div>
				<a class="footer__email" href="mailto:romnatik@ukr.net">romnatik@ukr.net</a>
				<a class="footer__phone" href="tel:+380503345487">+38 (050) 334-54-87‬</a>‎
			</div>
			<div>
				<a class="footer__email" href="mailto:wdcalukraine@ukr.net">wdcalukraine@ukr.net</a>
				<a class="footer__phone" href="tel:+380504730066">+38 (050) 473-00-66</a>‎‎
			</div>
		</address>
	</div>
	<p class="footer__copy">Copyright © StarLight. All rights reserved</p>
</footer>

<script src="../js/lib/jquery-3.1.1.min.js"></script>
<script src="../js/lib/fotorama.js"></script>
<script src="../js/lib/isotope.pkgd.min.js"></script>
<script src="../js/lib/jquery.fancybox.min.js"></script>
<script src="../js/lib/imagesloaded.pkgd.min.js"></script>
<script src="../js/main.js?v=<?=$ver?>"></script>