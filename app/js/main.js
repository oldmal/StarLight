$(document).ready(function(){
	"use strict";

	if ( $( window ).width() >= 1200 ){
		// $('.index-page').addClass('page-wrap--hidden');
		showMainBlock();
	}
	// show page after some delay to allow better user experience
	function showMainBlock() {
		setTimeout( function(){
			$('.page-wrap').removeClass('page-wrap--hidden');
		}, 150);
	}

	
	// show/hide menu
	$(document).on('click', '.header__nav-icon', function(event) {
		$('.header__nav').toggleClass('header__nav--hidden');
		$('.header__nav-icon').toggleClass('open');

		// hide menu by click outside of block
		$('body').on('click', '.page-wrap' ,function(e){
			var container = $(".header__nav-icon");
			if (!container.is(e.target) && container.has(e.target).length === 0) {
				$('.header__nav').addClass('header__nav--hidden');
				$('.header__nav-icon').removeClass('open');
			}
		});
	});
	$(document).on('click', '.header__nav-item', function() {
		$('.header__nav').addClass('header__nav--hidden');
		$('.header__nav-icon').removeClass('open');
	});

	// ================ START LANG CONTROL ================
	// add/remove active class to clicked btn
	$(document).on('click', '.lang__item', function() {
		if (!$(this).hasClass('lang__item--active')) {
			$('.lang__item').removeClass('lang__item--active');
			$(this).addClass('lang__item--active');
			var $this = $(this);
			changePage($this);
		}
	});
	function changePage($this) {
		var pageName = /[^/]*$/.exec(location)[0];
		if ($this.data('lang') === 'uk') {
			location = "../uk/" + pageName + "";
			// console.log( "uk/" + pageName + "" );
		} else {
			location = "../en/" + pageName + "";
			// console.log( "en/" + pageName + "" );
		}
	}
	// ================ END LANG CONTROL ================
	


	// ================ START INDEX ================
	function competFotoramaInit() {
		// FOR Mobile do not intit fotorama
		if ( $( window ).width() <= 767 ){
			$( ".compet__slides").fotorama({
				width: '100%',
				height: '615px',
				nav: false,
			});
		// FOR tablets
		} else if ( $( window ).width() <= 945 && $( window ).width() >= 768) {

		 // FOR Desctop
		} else {
			$( ".compet__slides").fotorama({
				width: '100%',
				height: '615px',
				nav: false,
			});
		}
	}
	competFotoramaInit();


	function scheduleFotoramaInit() {
		// FOR Mobile do not intit fotorama
		if ( $( window ).width() <= 767 ){
			$( ".schedule__gal").fotorama({
				width: '100%',
				height: 'auto',
				nav: false,
			});
		// FOR tablets
		} else if ( $( window ).width() <= 945 && $( window ).width() >= 768) {

		 // FOR Desctop
		} else {
			$( ".schedule__gal").fotorama({
				width: '100%',
				height: 'auto',
				nav: false,
			});
		}

		// if ( $( window ).width() <= 767 ){
		// 	$( ".schedule__gal").fotorama({
		// 		width: '1200px',
		// 		height: 'auto',
		// 		nav: false,
		// 	});
		// // FOR tablets
		// } else if ( $( window ).width() <= 945 && $( window ).width() >= 768) {

		//  // FOR Desctop
		// } else {
		// 	$( ".schedule__gal").fotorama({
		// 		width: '1200px',
		// 		height: 'auto',
		// 		nav: false,
		// 	});
		// }
	}
	scheduleFotoramaInit();


	// // initialize custom scrollbar
	// $(".schedule__items").mCustomScrollbar({
	// 	autoHideScrollbar: true,
	// });

	// ================ END INDEX ================
	

	// // scroll to next section by click smoth
	// $('.nav__item-link').on('click', function(e) {
	// 	e.preventDefault();
	// 	$('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 600, 'linear');
	// });

	function addGoogleMaps() {
		// Add Google Maps. Work with src that we add in html, depend.
		var myCenter = new google.maps.LatLng(50.450386, 30.443437);
		function initialize() {
			var mapProp = {
				center:myCenter,
				zoom:15,
				scrollwheel:false,
				mapTypeId:google.maps.MapTypeId.ROADMAP
			};
			var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
			var marker = new google.maps.Marker({
				position:myCenter,
			});
			marker.setMap(map);
		}
		google.maps.event.addDomListener(window, 'load', initialize);
	}
	if ($('.page-wrap').hasClass('location-page')) {
		addGoogleMaps();
	}



	// ================ START TRAINING ================
	// запустить функцию только на странице галереи
	if ($('.page-wrap').hasClass('training-page')) {
		// с помощью плагина определяем когда загрузились все фото и инициализируем грид
		$('#tngGallery').imagesLoaded().done( function( instance ) {
			initTngScrips();
			$('#tngGallery').removeClass('tng__gallery--hidden');
		});
	}
	// инициализация изотоп грида
	function initTngScrips() {
		$('#tngGallery').isotope({
			itemSelector: '#tngGallery .grid-item',
			masonry: {
				columnWidth: 360,
				gutter: 30,
			},
		});
	}
	// ================ END TRAINING ================



	// ================ START GALLERY ================

	// запустить функцию только на странице галереи
	if ($('.page-wrap').hasClass('gallery-page')) {
		initGalleryScrips();
	}


	function initGalleryScrips() {
		// ----------------- START GALLERY COMMON -----------------

		// // закрывать галлерею при тапе вне области картинки на мобильных 
		// if ( $( window ).width() <= 1200 ){
		// 	$(document).on('touchend', '.fancybox-slide', function() {
		// 		$.fancybox.close();
		// 		console.log('cat');
		// 	});
		// }

		var gal2017Loaded = false;
		var gal2018Loaded = false;
		var gal2019Loaded = false;
		// отобразить выбранную галерею
		function showSelectedGal($this) {
			if ($this.attr("id") == "gal__btn-photo-2016") {
				$('#gallPhoto2016').addClass('gall__unit--active');
				// дополнительная инициализация, потому как после скрытого состояния разрушиться грид
				initGal2016();
				if ($('html').attr('lang') === 'uk') {
					$('#galSubtitle').text('ФОТО 2016');
				} else if ($('html').attr('lang') === 'en') {
					$('#galSubtitle').text('PHOTO 2016');
				}
			} else if ($this.attr("id") == "gal__btn-photo-2017") {
				// проверим загружены ли фото, если да - убираем лоадер и отображаем гал
				if (gal2017Loaded) {
					$('#gallPhoto2017').addClass('gall__unit--active');
					// дополнительная инициализация, потому как после скрытого состояния разрушиться грид
					initGal2017();
				// иначе грузим галерею и после загрузки отборажаем ее когда сработает imagesLoaded()
				} else {
					$('.gal').addClass('gal--prior-loading');
				}
				if ($('html').attr('lang') === 'uk') {
					$('#galSubtitle').text('ФОТО 2017');
				} else if ($('html').attr('lang') === 'en') {
					$('#galSubtitle').text('PHOTO 2017');
				}
			} else if ($this.attr("id") == "gal__btn-photo-2018") {
				// проверим загружены ли фото, если да - убираем лоадер и отображаем гал
				if (gal2018Loaded) {
					$('#gallPhoto2018').addClass('gall__unit--active');
					// дополнительная инициализация, потому как после скрытого состояния разрушиться грид
					initGal2018();
				// иначе грузим галерею и после загрузки отборажаем ее когда сработает imagesLoaded()
				} else {
					$('.gal').addClass('gal--prior-loading');
				}
				if ($('html').attr('lang') === 'uk') {
					$('#galSubtitle').text('ФОТО 2018');
				} else if ($('html').attr('lang') === 'en') {
					$('#galSubtitle').text('PHOTO 2018');
				}
			} else if ($this.attr("id") == "gal__btn-photo-2019") {
				// проверим загружены ли фото, если да - убираем лоадер и отображаем гал
				if (gal2019Loaded) {
					$('#gallPhoto2019').addClass('gall__unit--active');
					// дополнительная инициализация, потому как после скрытого состояния разрушиться грид
					initGal2019();
				// иначе грузим галерею и после загрузки отборажаем ее когда сработает imagesLoaded()
				} else {
					$('.gal').addClass('gal--prior-loading');
				}
				if ($('html').attr('lang') === 'uk') {
					$('#galSubtitle').text('ФОТО 2019');
				} else if ($('html').attr('lang') === 'en') {
					$('#galSubtitle').text('PHOTO 2019');
				}

			} else if ($this.attr("id") == "gal__btn-video-2016") {
				$('#gallVideo2016').addClass('gall__unit--active');
				if ($('html').attr('lang') === 'uk') {
					$('#galSubtitle').text('ВІДЕО 2016');
				} else if ($('html').attr('lang') === 'en') {
					$('#galSubtitle').text('VIDEO 2016');
				}
			}  else if ($this.attr("id") == "gal__btn-video-2017") {
				$('#gallVideo2017').addClass('gall__unit--active');
				if ($('html').attr('lang') === 'uk') {
					$('#galSubtitle').text('ВІДЕО 2017');
				} else if ($('html').attr('lang') === 'en') {
					$('#galSubtitle').text('VIDEO 2017');
				}
			}  else if ($this.attr("id") == "gal__btn-video-2018") {
				$('#gallVideo2018').addClass('gall__unit--active');
				if ($('html').attr('lang') === 'uk') {
					$('#galSubtitle').text('ВІДЕО 2018');
				} else if ($('html').attr('lang') === 'en') {
					$('#galSubtitle').text('VIDEO 2018');
				}
			}  else if ($this.attr("id") == "gal__btn-video-2019") {
				$('#gallVideo2019').addClass('gall__unit--active');
				if ($('html').attr('lang') === 'uk') {
					$('#galSubtitle').text('ВІДЕО 2019');
				} else if ($('html').attr('lang') === 'en') {
					$('#galSubtitle').text('VIDEO 2019');
				}
			}

		}

		// переключение между gallery/video
		$(document).on('click', '.gal__btn', function() {
			// set active btn
			$('.gal__btn').removeClass('gal__btn--active');
			$(this).addClass('gal__btn--active');
			// clean active class
			$('.gall__unit').removeClass('gall__unit--active');
			var $this = $(this);
			showSelectedGal($this);
		});
		// ----------------- END GALLERY COMMON -----------------

		// с помощью плагина определяем когда загрузились все фото и инициализируем грид
		$('#gallPhoto2016').imagesLoaded().done( function( instance ) {
			$('.gal').removeClass('gal--loading');
			// отобразить данную глерею, если на момент загрузки ее именно и ожидает пользователь
			if ( $('#gal__btn-photo-2016').hasClass('gal__btn--active') ) {
				$('#gallPhoto2016').addClass('gall__unit--active');
				initGal2016();
			}
		});

		// инициализация грида 2016
		function initGal2016() {
			$('#gallPhoto2016').isotope({
				itemSelector: '#gallPhoto2016 .grid-item',
				masonry: {
					columnWidth: 360,
					gutter: 30,
				}
			});
		}
		// с помощью плагина определяем когда загрузились все фото и инициализируем грид
		$('#gallPhoto2017').imagesLoaded().done( function( instance ) {
			// console.log('2017 all images successfully loaded');
			// upd var
			gal2017Loaded = true;
			// если к тому времени как загрузилась галерея нажали на 2017 галерею,
			// спрятать лоадер и отобразить грид галереи
			if ( $('#gal__btn-photo-2017').hasClass('gal__btn--active') ) {
				// после того как галерея display block, можно инитить галерею
				$('#gallPhoto2017').addClass('gall__unit--active');
				initGal2017();
				// hide loader
				$('.gal').removeClass('gal--prior-loading');
			}
		});

		// инициализация грида 2017
		function initGal2017() {
			$('#gallPhoto2017').isotope({
				itemSelector: '#gallPhoto2017 .grid-item',
				masonry: {
					columnWidth: 360,
					gutter: 30,
				}
			});
		}

		// с помощью плагина определяем когда загрузились все фото и инициализируем грид
		$('#gallPhoto2018').imagesLoaded().done( function( instance ) {
			// console.log('2018 all images successfully loaded');
			// upd var
			gal2018Loaded = true;
			// если к тому времени как загрузилась галерея нажали на 2018 галерею,
			// спрятать лоадер и отобразить грид галереи
			if ( $('#gal__btn-photo-2018').hasClass('gal__btn--active') ) {
				// после того как галерея display block, можно инитить галерею
				$('#gallPhoto2018').addClass('gall__unit--active');
				initGal2018();
				// hide loader
				$('.gal').removeClass('gal--prior-loading');
			}
		});

		// инициализация грида 2018
		function initGal2018() {
			$('#gallPhoto2018').isotope({
				itemSelector: '#gallPhoto2018 .grid-item',
				masonry: {
					columnWidth: 360,
					gutter: 30,
				}
			});
		}

		// с помощью плагина определяем когда загрузились все фото и инициализируем грид
		$('#gallPhoto2019').imagesLoaded().done( function( instance ) {
			// console.log('2018 all images successfully loaded');
			// upd var
			gal2019Loaded = true;
			// если к тому времени как загрузилась галерея нажали на 2018 галерею,
			// спрятать лоадер и отобразить грид галереи
			if ( $('#gal__btn-photo-2019').hasClass('gal__btn--active') ) {
				// после того как галерея display block, можно инитить галерею
				$('#gallPhoto2019').addClass('gall__unit--active');
				initGal2019();
				// hide loader
				$('.gal').removeClass('gal--prior-loading');
			}
		});

		// инициализация грида 2018
		function initGal2019() {
			$('#gallPhoto2019').isotope({
				itemSelector: '#gallPhoto2019 .grid-item',
				masonry: {
					columnWidth: 360,
					gutter: 30,
				}
			});
		}


	}
	//----------------- END GALLERY 2017 -----------------




	// ================ END GALLERY ================





});




